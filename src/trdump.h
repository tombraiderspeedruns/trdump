/*
 *  Copyright (C) 2017-2018 DarenK
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <inttypes.h>

// Enum for Memory Card/Savegame dump
enum {
	TR1_PC = 0x1,
	TR1_PS,
	TR2_PC,
	TR2_PS,
	TR3_PC,
	TR3_PS
};

// Enum for Debugging
enum {
	TR2_ID,
	TR3_ID,
};

const char *tr_exe_names[] = {
	"tomb2.exe",
	"tomb3.exe"
};

#define SAVE_SIZE_TR1_PC 10675
#define SAVE_SIZE_TR1_PS 8192
#define SAVE_SIZE_TR2_PC 7442
#define SAVE_SIZE_TR2_PS 8192
#define SAVE_SIZE_TR3_PC 13913
#define SAVE_SIZE_TR3_PS 16384

unsigned char tr1_ps_signature[] = {
	0x53, 0x43, 0x11, 0x01, 0x82, 0x73, 0x82, 0x8f, 0x82, 0x8d, 0x82, 0x82, 0x81, 0x40, 0x82, 0x71,
	0x82, 0x81, 0x82, 0x89, 0x82, 0x84, 0x82, 0x85, 0x82, 0x92, 0x00
};

unsigned char tr2_ps_signature[] = {
	0x53, 0x43, 0x11, 0x01, 0x82, 0x73, 0x82, 0x8f, 0x82, 0x8d, 0x82, 0x82, 0x81, 0x40, 0x82, 0x71,
	0x82, 0x81, 0x82, 0x89, 0x82, 0x84, 0x82, 0x85, 0x82, 0x92, 0x81, 0x40, 0x82, 0x68, 0x82, 0x68,
	0x00
};

unsigned char tr3_ps_signature[] = {
	0x53, 0x43, 0x11, 0x02, 0x82, 0x73, 0x82, 0x8f, 0x82, 0x8d, 0x82, 0x82, 0x81, 0x40, 0x82, 0x71,
	0x82, 0x81, 0x82, 0x89, 0x82, 0x84, 0x82, 0x85, 0x82, 0x92, 0x81, 0x40, 0x82, 0x68, 0x82, 0x68,
	0x82, 0x68, 0x00
};

const char *tr2_levelnames[] = {
	"The Great Wall",
	"Venice",
	"Bartoli's Hideout",
	"Opera House",
	"Offshore Rig",
	"Diving Area",
	"40 Fathoms",
	"Wreck of the Maria Doria",
	"Living Quarters",
	"The Deck",
	"Tibetan Foothills",
	"Barkhang Monastery",
	"Catacombs of the Talion",
	"Ice Palace",
	"Temple of Xian",
	"Floating Islands",
	"The Dragon's Lair",
	"Home Sweet Home",
	"Total In-Game Time"
};

const char *tr3_levelnames[] = {
	"Jungle",
	"Temple Ruins",
	"The River Ganges",
	"Caves of Kaliya",
	"Coastal Village",
	"Crash Site",
	"Madubu Gorge",
	"Temple of Puna",
	"Thames Wharf",
	"Aldwych",
	"Lud's Gate",
	"City",
	"Nevada Desert",
	"High Security Compound",
	"Area 51",
	"Antarctica",
	"RX-Tech Mines",
	"Lost City of Tinnos",
	"Meteorite Cavern",
	"Total In-Game Time"
};

const char *separator = "=======================================";

// Offsets to the Levelstats
#define OFFSET_LS_TR2_PC 0x7b
#define OFFSET_LS_TR2_PS 0x22c

enum {
	TR2_SECRET_GOLD = 0x1,
	TR2_SECRET_JADE = 0x2,
	TR2_SECRET_STONE = 0x4
};

enum {
	TR2_WEAPON_UNARMED = 0x1,
	TR2_WEAPON_PISTOLS = 0x2,
	TR2_WEAPON_AP = 0x4,
	TR2_WEAPON_UZI = 0x8,
	TR2_WEAPON_SG = 0x10,
	TR2_WEAPON_M16 = 0x20,
	TR2_WEAPON_GL = 0x40,
	TR2_WEAPON_HG = 0x80
};

// TR2 states for trdump_set_state_tr2
enum {
	TR2_STATE_PRE_QWOP = 0x0,
	TR2_STATE_PRE_QWOP_POS1 = 0x1,
	TR2_STATE_PRE_QWOP_POS2 = 0x2,
	TR2_STATE_PRE_QWOP_POS3 = 0x3,
	TR2_STATE_PRE_QWOP_POS4 = 0x4,
	TR2_STATE_PRE_QWOP_POS5 = 0x5,
	TR2_STATE_PRE_QWOP_POS6 = 0x6,
	TR2_STATE_PRE_QWOP_POS7 = 0x7,
	TR2_STATE_PRE_QWOP_POS8 = 0x8,

	TR2_STATE_QWOP = 0x10,

	TR2_STATE_ONG = 0x20,
	TR2_STATE_ONG_1 = 0x21,
	TR2_STATE_ONG_2 = 0x22,
	TR2_STATE_ONG_3 = 0x23,
	TR2_STATE_ONG_4 = 0x24,
	TR2_STATE_ONG_5 = 0x25,
	TR2_STATE_ONG_6 = 0x26,
	TR2_STATE_ONG_7 = 0x27,
	TR2_STATE_ONG_8 = 0x28
};

#pragma pack(push, 1)
struct levelstats_tr2 {
	uint16_t health;
	uint16_t ap_ammo;
	uint16_t uzi_ammo;
	uint16_t sg_ammo;
	uint16_t m16_ammo;
	uint16_t gl_ammo;
	uint16_t hg_ammo;
	uint8_t small_med_count;
	uint8_t large_med_count;
	uint8_t bytes_1;
	uint8_t flare_count;
	uint8_t bytes_2[2];
	uint8_t weapon_flag;
	uint8_t bytes_[3];
	uint32_t timer;
	uint32_t ammo_used;
	uint32_t hits;
	uint32_t distance_travelled;
	uint16_t kills;
	uint8_t secrets;
	uint8_t meds_used; // 1 = single medipack, 2 = large medipack
};
#pragma pop

#ifdef __linux__
#pragma pack(push, 1)
struct larainfo_tr2 {
	int32_t ground;			// 0x00
	uint32_t interaction;	// 0x04
	uint16_t visibility;	// 0x08
	uint8_t gap_1[2];		// 0x0a
	uint16_t id;			// 0x0c
	uint16_t action;		// 0x0e
	uint16_t next_action;	// 0x10
	uint8_t gap_2[4];		// 0x12
	uint8_t anim_frame;		// 0x13
	uint8_t animation;		// 0x14
	uint16_t room;			// 0x16
	int16_t last_entity;
	uint8_t gap_3[2];
	int16_t hspeed;
	int16_t vspeed;
	int16_t health;
	uint8_t gap_4[16];
	int32_t x;
	int32_t y;
	int32_t z;
	uint16_t pitch;
	uint16_t yaw;
	uint16_t roll;
	uint16_t air;
};
#pragma pop
#endif

#ifdef __linux__
#pragma pack(push, 1)
struct larainfo_tr3 {
	int32_t yPosGround;
	int32_t interaction;
	int32_t meshBits; // <-- this one is 32 bits
	int16_t entityID;
	int16_t currentAction;
	int16_t nextAction;
	int16_t requiredAction; // <-- required end action
	int16_t animNumber; // <-- index for animation array
	uint8_t animFrame;
	uint8_t currAnim;
	int16_t currRoomNum;
	int16_t LastActiveEntityIndex;
	int16_t nextActive;
	int16_t hspeed;
	int16_t vspeed;
	int16_t health;
	int16_t tileRelated;
	int16_t timer;
	int16_t stateFlags;
	int16_t shade; // dynamic lighting has 0xFFFF value
	//void* data;
	uint8_t stuff[20];
	int32_t x;
	int32_t y;
	int32_t z;
	uint16_t pitch;
	uint16_t yaw;
	uint16_t roll;
	uint16_t flags; // isActive|pad|pad|gravity|bulletHit|collidable|hasBeenSeen
};
#pragma pop
#endif

int trdump_find_tr_savegame_signature(char *filename, unsigned int *offset);
int trdump_tr_igt(char *filename, int tr_id, unsigned int offset);
int trdump_read_tr2_igt(char *split_key);
void trdump_get_time_string_from_ticks(uint32_t time_ticks, char *time_string);
int trdump_read_tr2_larainfo(char *logfile);
int trdump_read_tr3_larainfo(char *logfile);
int trdump_set_state_tr2(int state);
int trdump_get_tr_pid(int tr_id);
